// console.log("JavaScript DICE");

// console.log("You rolled the dice and the result is:");

// function rollDice() {
//   return Math.trunc(Math.random() * 6) + 1;
// }
// let diceRoll = rollDice();
// console.log(diceRoll);


let friends = ["Chandler", "Joey", "Ross", "Phoebe"];
console.log(friends);

let newfriends = friends.push("Rachel"); //add last
console.log(friends);
console.log(newfriends);

friends.unshift("Monica"); //add first
friends.unshift("Barry");

friends.push("Janice");
console.log(friends);

friends.pop(); //remove last
console.log(friends);

friends.shift(); //remove first
console.log(friends);

// friends.splice(3, 5, "XXX", "YYY", "ZZZ"); //startingindex, deletecont, elements to be added,
// console.log(friends);

friends.sort(); // sort alphabetically
console.log(friends);

friends.reverse();
console.log(friends);

console.log(friends.indexOf("Ross")); // check position from start
// friends.push("Ross");

console.log(friends);
console.log(friends.lastIndexOf("Ross")); // checks from last position

let pos = friends.indexOf("Ross");
console.log(`Ross' position is ${pos}`);

console.log(friends.includes("Gunther")); // check inclusion x
let isMemberOf = friends.includes("Gunther");
console.log(isMemberOf);

let broke = friends.slice(5);
console.log(broke);

// toString
let stringFriends = friends.toString(); // convert to single string
console.log(stringFriends);
console.log(stringFriends.length);
console.log(typeof stringFriends);

// concat()

let bigbang = ["Sheldon", "Leonard", "Howard", "Rajesh", "Penny", "Amy", "Bernadette"]
let himym = ["Marshal", "Barney", "Ted", "Lily", "Robin"]
let casts = bigbang.concat(friends); // merge arrays
console.log(casts);
let allCasts = bigbang.concat(friends, himym); //merge multiple arrays
console.log(allCasts);

// join()

console.log(himym);
console.log(himym.join()); // returns as strings separated by string separators
console.log(himym.join('')); // no separator
console.log(himym.join('xxx')); // xxx as separator or any character

//forEach
let faveCast = [];
allCasts.forEach(function(cast){ // lists all values
  console.log(cast)
})

allCasts.forEach(function(cast){
if (cast.length > 5){
  faveCast.push(cast)
}
})
console.log(faveCast);

let x = [1,2,3,4,5];

let mapNum = x.map(function(x){
  return x + x * x / x;
})
console.log(mapNum);

//every
let everyNum = x.every(function(x){ //checks if ALL pass condition
  return (x < 3);
})
console.log(everyNum);

let someNum = x.some(function(x){ //checks if at least 1 passes the condition
  return (x < 2);
})
console.log(someNum);

let filtervalid = x.filter(function(x){ // filters onl
  return (x < 3);
})
console.log(filtervalid);
// let ross = {
//   firstName: 'Ross',
//   lastName: 'Geller',
//   job: 'paleontologist',
//   hasDriversLicense: true,
//   birthYear: 1986,
//   calcAge: function () {
//     this.age = 2022 - this.birthYear;
//     return this.age;
//   }
// };
// console.log(typeof ross);
// console.log(ross);
// console.log(ross.calcAge());
// console.log(ross.age);